# spip-postgres

## En travaux

``` /!\ Ce docker n'est pas fonctionnel ```

Pour l'instant, spip n'est pas compatible avec postgresql. Ce projet permet à nos équipe de tester la compabilité.

Spip fonctionnait autrefois avec cette base de donnée, et la configuration d'une tel SGBD est toujours possible avec un peu de bidouille, mais le code a évolué pendant des années sans maintenance pgsql, et aujourd'hui, le fonctionnement de posgresql est buggé.

Ce projet reste toutefois intéressant pour illustrer un fonctionnement possible avec un docker-compose, à partir du dockerfile de ipeos.

## Objectif

Ce dépôt est le code permettant de créer un docker spip paramétré pour l'utilisation d'une base PostrgreSQL, de secrets docker et d'un serveur smtp distant.

Le docker-compose.yml ne sert qu'à builder le dépôt. Ses paramètres, notamment les secrets permet d'illustrer un usage avec des secrets docker.

### Secret docker ( XXX_FILE )

L'utilisation de secret docker avec les variables d'environnement en `_FILE` est disponible et fonctionnelle pour les base mysql.

Les variables d'environnement concernées :

- SPIP_DB_SERVER
- SPIP_DB_HOST
- SPIP_DB_LOGIN
- SPIP_DB_PASS
- SPIP_DB_NAME
- SPIP_DB_PREFIX
- SPIP_SITE_ADDRESS
- SPIP_ADMIN_NAME
- SPIP_ADMIN_LOGIN
- SPIP_ADMIN_EMAIL
- SPIP_ADMIN_PASS

#### fonctionnement

Les variables en _FILE sont prioritaire sur les variables correspondantes.
Toutefois, elles ne sont prise en compte que si :

1. Les variables sont définies et non vide
2. Un fichier existe bel et bien à l'adresse indiquée
3. Ce fichier n'est pas vide.

Dans le cas contraire, c'est la variable sans _FILE qui est utilisée. ( avec leurs valeurs par défaut si elles ne sont pas définies )

## Codes utilisés et modifiés

* **docker-entrypoint.sh**
  Le code d'origine est celui de [ipeos](https://hub.docker.com/r/ipeos/spip/), récupéré dans leur docker. Auquel on a ajouté :
  * une fonction permettant de lire les secrets docker (variable en `_FILE`)
  * la modification du nom d'un fichier source de spip pour utiliser les bases postgresql. ( `ecrire/req/pg.exp.php` -> `ecrire/req/pg.php` )
